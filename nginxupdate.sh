#!/bin/bash

DOMAINS=$1
FILES=$2
IFS=","

for domain in $DOMAINS
do
        for file in $FILES
        do

                printf "\n\n\n\n backup configuration file $file ... \n\n\n\n\n"

                cp /etc/nginx/sites-available/$file /etc/nginx/sites-available/"$file"backup

                printf "\n\n\n\n adding domain $domain to file $file ... \n\n\n\n\n"


                sed -i 's/server_name/server_name '"$domain"'/' /etc/nginx/sites-available/$file

                if grep -q "syntax is ok" <(nginx -t 2>&1); then

                        echo "success"
                        cat /etc/nginx/sites-available/$file
                        rm  /etc/nginx/sites-available/"$file"backup
                        sudo systemctl reload nginx

                else

                        echo "error occured, reverting"
                        rm /etc/nginx/sites-available/$file
                        mv /etc/nginx/sites-available/"$file"backup /etc/nginx/sites-available/$file

                fi
        done
done
