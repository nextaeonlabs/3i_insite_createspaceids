#!/bin/sh

PORT=3050
URL=http://localhost:$PORT
USERNAME=$4 #3imaster
PASSWORD="h9d&1nKqV4g" #1234qwer
VENDOR=$3 #test
NAME=$1 #testsam
DISPLAYNAME=$2 #TESTSAM
TARGETSERVER=$6 #TARGET SERVER
EXECUTEDEPLOY=$7

printf "name: $NAME \n"
printf "displayname: $DISPLAYNAME \n"
printf "vendor: $VENDOR \n"
printf "username: $USERNAME \n"
printf "password: $PASSWORD \n"
printf "target server: $TARGETSERVER \n"
printf "deploy: $EXECUTEDEPLOY \n"


printf "\n\n\n\n check if jq installed ... \n\n\n\n\n"

if ! command -v jq &> /dev/null
then
    echo "jq could not be found, installing now"
    sudo apt install jq -y
fi



printf "\n\n\n\n performing login to the server at $URL ... \n\n\n\n\n"

TOKEN=$(curl -X POST "$URL/aun/p/authn/login?vendor=$VENDOR" -H 'Content-Type: application/json' -d'{
	"username": "'"$USERNAME"'",
	"password": "'"$PASSWORD"'"
}' | jq -r '.data.tokens.accessToken'
) 


echo "$TOKEN"


printf "\n\n\n\n creating new vendor: $VENDOR ... \n\n\n\n\n"

RESPONSE=$(curl -X POST "$URL/vnd/vendors" -H 'Authorization: '"$TOKEN"'' -H 'Content-Type: application/json' \
-d '{
	"name": "'"$NAME"'",
	"displayName": "'"$DISPLAYNAME"'"
}'
) 

echo "$RESPONSE"
